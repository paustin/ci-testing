#!/bin/bash
# From https://github.com/srcmake/sample-gitlabci-cpp-project/blob/master/verify.sh
echo "Starting sample CI verification script"

echo "Trying to execute ./mybinary"

OUTPUT=`./mybinary`
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
  echo "Retval is 0, OK"
else
  echo "Retval is not 0, FAIL"
  exit 1
fi

if [ "$OUTPUT" == "Hello World" ]; then
  echo "Output is correct, OK"
else
  echo "Expected Hello World but got $OUTPUT instead, FAIL"
  exit 1
fi